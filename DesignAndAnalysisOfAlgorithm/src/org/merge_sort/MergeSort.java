package org.merge_sort;

public class MergeSort {

	
	private int[] helper;
	private int[] numbers;
	private int number; 
	private static int inversion;
	
	public void sort(int[] values) {
		inversion = 0;
		this.numbers = values;
		number = values.length;
		this.helper = new int[number];
		mergesort(0, number-1, 0);
	}
	
	private void mergesort(int low, int high, int parse) {

		if (low < high) {
			int middle = (low + high)/2 ;
			mergesort(low, middle, parse+1 );
			mergesort(middle+1, high, parse+1 );
			merge(low, middle, high);
		}
	}
	
	private void merge(int low, int middle, int high) {
		for (int i = low; i <=high; i++) {
			helper[i] = numbers[i];
		}
		
		int i = low;
		int j = middle+1;
		int k = low;
		
		while (i <= middle && j <= high) {
			if (helper[i] <= helper[j]) {
				numbers[k] = helper[i];
				i++;
			}
			else {
				numbers[k] = helper[j];
				inversion = (middle+1 - i) + inversion;
				j++;
			}
			k++;
		}
		while (i <= middle) {
			numbers[k] = helper[i];
			k++;
			i++;
		}
	}
	
	public static void main(String[] args) {
		MergeSort ms = new MergeSort();
		int[] numbers = {2,4,1,3,5};
		ms.sort(numbers);

		for (int i = 0; i < numbers.length ; i++) {
			System.out.print(numbers[i] + " ");
		}
	}

}
