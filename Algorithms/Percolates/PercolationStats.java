public class PercolationStats {
    private Percolation p;
    private double mean = 0;
    private double stdDev = 0;
    private double minConfInterval = 0;
    private double maxConfInterval = 0;
    private double[] percolateValues;
    private double percolateValue = 0;
    private int randomRowSelect;
    private int randomColumnSelect;
    
    public PercolationStats(int N, int T)    
// perform T independent computational experiments on an N-by-N grid
    {
        validationIndeces(N, T);
        percolateValues = new double[T];
        for (int i = 0; i < T; ++i) {
            p = new Percolation(N);
            while (!p.percolates()) {
                setRandomRowSelect(StdRandom.uniform(N) + 1);
                setRandomColumnSelect(StdRandom.uniform(N) + 1);
                if (!p.isOpen(getRandomRowSelect(), getRandomColumnSelect())) {
                    p.open(getRandomRowSelect(), getRandomColumnSelect());
                    setPercolateValue(1);
                }
            }
            percolateValues[i] = getPercolateValue()/(double) (N*N);
            setPercolateValue(0);
        }
    }
    
    private void validationIndeces(int N, int T) {
        if (N <= 0 || T <= 0) throw 
            new IllegalArgumentException("dimen or no. of experiment error");
    }
    
    private double getPercolateValue() {
        return percolateValue;
    }
    
    private int getRandomRowSelect() {
        return randomRowSelect;
    }
    
    private int getRandomColumnSelect() {
        return randomColumnSelect;
    }
    
    private void setPercolateValue(int i) {
        if (i > 0) ++percolateValue;
        else percolateValue = 0;
    }
    
    private void setRandomRowSelect(int i) {
        randomRowSelect = i;
    }
    
    private void setRandomColumnSelect(int i) {
        randomColumnSelect = i;
    }
    
    private double getMean() {
        return mean;
    }
    private void setMean(double i) {
        mean = i;
    }
    private double getStddev() {
        return stdDev;
    }
    private void setStddev(double i) {
        stdDev = i;
    }
    private double getMinConfInterval() {
        return minConfInterval;
    }
    private void setMinConfInterval(double i) {
        minConfInterval = i;
    }
    private double getMaxConfInterval() {
        return maxConfInterval;
    }
    private void setMaxConfInterval(double i) {
        maxConfInterval = i;
    }
    
    
    public double mean()                    // sample mean of percolation threshold
    {
        setMean(StdStats.mean(percolateValues));
        return getMean();
    }
    public double stddev()    // sample standard deviation of percolation threshold
    {
        setStddev(StdStats.stddev(percolateValues));
        return getStddev();
    }
    public static void main(String[] args)   // test client, described below
    {
        int N = 0;
        int T = 0;        
        if (args.length > 0) {
            try {
                N = Integer.parseInt(args[0]);
                T = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                System.err.print("Number format incorrect !");
            }
            //System.out.println (N) ;
            //System.out.println (T) ;
        }
        
        PercolationStats pStats = new PercolationStats(N, T);
        System.out.println(pStats.mean());
        System.out.println(pStats.stddev());
    }
}