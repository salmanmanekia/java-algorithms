import org.junit.Test;
import org.junit.Before;
import org.junit.Ignore;
import static org.junit.Assert.*;
import java.util.Iterator;

public class DequeTest {
    
    private Deque<String> deque;
    
    @Before
    public void setUp() {
        deque = new Deque<String>();
    }
    
    @Test
    public void testIsEmpty() {
        assertTrue(deque.isEmpty());
    }
    
    @Test
    public void testSize() {
        assertTrue(deque.size() == 0);
        deque.addFirst("s");
        assertTrue(deque.size() == 1);
    }
    
    @Test
    public void testAddLast() {
        assertTrue(deque.size() == 0);
        deque.addLast("s");
        assertTrue(deque.size() == 1);
    }
    
    @Test
    public void testRemoveFirst() {
        assertTrue(deque.size() == 0);
        deque.addLast("s");
        assertTrue(deque.size() == 1);
        String item = deque.removeFirst();
        assertTrue(item == "s");
    }
    
    @Test
    public void testRemoveLast() {
        assertTrue(deque.size() == 0);
        deque.addFirst("s");
        assertTrue(deque.size() == 1);
        String item = deque.removeLast();
        assertTrue(item == "s");
    }
    
    @Test
    public void testIterator() {
        assertTrue(deque.size() == 0);
        deque.addFirst("s");
        assertTrue(deque.size() == 1);
        deque.addLast("t");
        deque.addLast("a");
        deque.addLast("r");
        deque.addLast("t");
        assertTrue(deque.size() == 5);
        Iterator<String> iter = deque.iterator();
        assertTrue(iter != null);
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == "s");
        assertTrue(iter.next() == "t");
        assertTrue(iter.next() == "a");
        assertTrue(iter.next() == "r");
        assertTrue(iter.next() == "t");
        assertFalse(iter.hasNext());
    }
    
    @Test (expected = java.lang.UnsupportedOperationException.class)
    public void testIteratorException() {
        Iterator<String> iter = deque.iterator();
        iter.remove();
    }
    
    @Test (expected = java.util.NoSuchElementException.class)
    public void testIteratorNextException() {
        Iterator<String> iter = deque.iterator();
        iter.next();
    }
    
    @Test (expected = java.lang.NullPointerException.class)
    public void testNullItemCheck() {
        deque.addFirst(null);
    }
    
    @Test (expected = java.util.NoSuchElementException.class)
    public void testEmptyDequeCheck() {
        deque.removeFirst();
    }
}